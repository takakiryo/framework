package fram.no1;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodReflection {

	public void doReflection(){
		String fooValue = "";
		String barValue = "";
		String bazValue = "";
		try {
			Foo foo = Foo.class.newInstance();
			Method method = Foo.class.getMethod("doFoo",String.class);

			//method実行
			String a = (String)method.invoke(foo, "aaa");

			System.out.println(a);

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}
}
