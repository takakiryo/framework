package Flywieght;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanInfo {
	private Map<String ,PropertyDescriptor> properties = new ConcurrentHashMap<String, PropertyDescriptor>();

	public PropertyDescriptor getPropertyDescriptor(String propertyName) {
		if (properties.containsValue(propertyName)) {
			return properties.get(propertyName);
		}
		PropertyDescriptor ret = new PropertyDescriptor();
		ret.setName(propertyName);
		properties.put(propertyName, ret);
		return ret;
	}
}
