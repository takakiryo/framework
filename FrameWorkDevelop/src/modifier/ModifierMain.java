package modifier;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ModifierMain {

	public static void main(String[] args) {

		try {
			//■methodの修飾子を調べる
			//オブジェクト取得
			Method method = Number.class.getMethod("doubleValue");
			int modifiers = method.getModifiers();
			//表示処理
			System.out.println(method);
			System.out.println("isAbstract :"
					+ Modifier.isAbstract(modifiers));
			System.out.println("isStatic :"
					+ Modifier.isStatic(modifiers));
			System.out.println("isPublic :"
					+ Modifier.isPublic(modifiers));
			System.out.println();

			//■String.classがインターフェースかどうか調査
			//オブジェクト取得
			Class<?> clazz = String.class;
			//表示処理
			System.out.println(clazz);
			System.out.println("String.class::isInterface" + Modifier.isInterface(clazz.getModifiers()));

		} catch (NoSuchMethodException e){
			e.printStackTrace();
		}

	}
}
