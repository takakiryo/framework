/*package Annotation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

public class AnnotationMain {

	public static void main(String[] args) {
		try {
			Method method = Foo.class.getMethod("foo", String.class, String.class);
			Annotation [][] parameterAnnotations = method
					.getParameterAnnotations();
			System.out.println(parameterAnnotations[0][0]);
			System.out.println(parameterAnnotations[1][0]);

		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static interface Foo {
		void foo (@NotNull String bar, @Nullable String baz);
	}

	@Target ( { PARAMETER } )
	@Retention(RUNTIME)
	public static @interface NotNull {

	}

	@Target ( { PARAMETER } )
	@Retention ( RUNTIME )
	public static @interface Nullable{

	}


}
*/